$(document).ready(function() {

    var $toggleButton = $('.toggle-button-right'),
        $menuWrap = $('.menu-wrap-right'),
        $sidebarArrow = $('.sidebar-menu-arrow-right');

    // Hamburger button

    $toggleButton.on('click', function() {
        // $(this).toggleClass('button-open-right');
        $menuWrap.toggleClass('menu-show-right');
    });

    $("#closeContact").on('click', function() {
        // $(this).toggleClass('button-open-right');
        $menuWrap.toggleClass('menu-show-right');
    });
    // Sidebar navigation arrows

    $sidebarArrow.click(function() {
        $(this).next().slideToggle(600);

    });

});