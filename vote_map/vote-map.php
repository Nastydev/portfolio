<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
    $(function () {
        $("#draggable").draggable({
            appendTo: 'body',
            containment: 'window',
            scroll: false,
        });

        $("#delete").click(function () {
            $("#draggable").remove()
        })

        $("#vote").click(function () {
            var radioValue = $("input[name='voteMap']:checked").val();

            var sucessCss = {
                color: "#000",
                fontWeight: "bolder",
                fontSize : "25px",
                display: "flex",
                justifyContent: "center",
                height: "100%",
                alignItems: "center",
            }

            var sucess =$("#sucess")
            var background = $("#draggable")
            console.log(radioValue);
            if (radioValue === "1") {
                sucess.empty().html("Arf, i'm really sorry ! ").css(sucessCss)
                background.css("background", "linear-gradient(0deg, rgba(34,19,0,1) 0%, rgba(255,142,0,1) 50%, rgba(34,19,0,1) 100%)")
            }
            if (radioValue === "2"){
                sucess.empty().html("Thanks you ! ").css(sucessCss)
                background.css("background", "linear-gradient(0deg, rgba(34,19,0,1) 0%, rgba(255,142,0,1) 50%, rgba(34,19,0,1) 100%)")
            }
            if (radioValue === "3"){
                sucess.empty().html("Oh sh.. <br> Nastydev is born then ! ").css(sucessCss)
                background.css("background", "linear-gradient(0deg, rgba(34,19,0,1) 0%, rgba(255,142,0,1) 50%, rgba(34,19,0,1) 100%)")
            }

            $.ajax({
                method: "POST",
                url: "../vote_map/check-vote.php",
                data: {
                    voteMap: radioValue,
                },
            })


        })
    })
</script>

<div id="draggable">
    <button id="delete" type="button">X</button>
    <div id="sucess">
        <p>Hey boys and girls ! <br>
            What do you think about this Portfolio?</p>

        <label for="bad">Bad</label>
        <input type="radio" value="1" id="bad" name="voteMap">

        <label for="better">You can do better</label>
        <input type="radio" value="2" id="better" name="voteMap">

        <label for="good">Good</label>
        <input type="radio" value="3" id="good" name="voteMap">

        <input type="submit" name="vote" id="vote" value="VOTE">
    </div>
</div>

