<?php
require "../database/connexion.php";


if (isset($_POST['voteMap']) AND is_numeric($_POST['voteMap'])) { //si ajax à une valeur pour voteMap(une radio)

    $voteMap = $_POST['voteMap'];

    $reqValVote = $bdd->prepare("SELECT * FROM vote where id = :id");
    $reqValVote->execute(array(
        'id' => $voteMap
    ));
    $totalVote = $reqValVote->fetch();

    $reqAddVote = $bdd->prepare("UPDATE `vote` SET `total_voted` = :total WHERE `vote`.`id` = :id;");
    $reqAddVote->execute(
        array(
            'id' => $voteMap,
            'total' => $totalVote["total_voted"] + 1,
        )
    );
    $reqCount = $reqAddVote->rowCount();
}

