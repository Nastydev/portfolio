<?php
session_start();

require '../functions/functions.php';
require '../database/connexion.php';
$errors = [];

// Vérifier si le formulaire a été soumis
if ($_SERVER["REQUEST_METHOD"] == "POST") {

    if (!empty($_POST['title']) and preg_match("/^[a-zA-Z0-9-àáâãäåçèéêëìíîïðòóôõöùúûüýÿ|_. ]+$/", $_POST['title'])) {
        $title = $_POST['title'];
        if (!empty($_POST['type'] and preg_match("/^[a-zA-Z ]+$/", $_POST['type']))) {
            $type = $_POST['type'];

            // Vérifie si le fichier a été uploadé sans erreur.
            if (isset($_FILES["photo"])) {
                $allowed = array("jpg" => "image/jpg", "jpeg" => "image/jpeg", "gif" => "image/gif", "png" => "image/png");
                $filename = $_FILES["photo"]["name"];
                $filetype = $_FILES["photo"]["type"];
                $filesize = $_FILES["photo"]["size"];

                if (preg_match("/^[a-zA-Z0-9._ -]+$/", $filename)) {
                    // Vérifie l'extension du fichier
                    $ext = pathinfo($filename, PATHINFO_EXTENSION);
                    if (array_key_exists($ext, $allowed)) {
                        // Vérifie la taille du fichier - 5Mo maximum
                        // Check file size
                        if ($filesize < 5242880) {

                            // Vérifie le type MIME du fichier
                            if (in_array($filetype, $allowed)) {
                                $newName = supExt($filename) . strtotime('now') . str_replace('image/', '.', $filetype);

                                $uploadImg = $bdd->prepare("INSERT INTO photos (`name`,`title`,`type`) VALUES (?,?,?)");
                                $uploadImg->execute(array($newName, $title, $type));

                                if (substr($type, 0, 4) == "logo" || substr($type, 0, 4) == "Logo") {
                                    move_uploaded_file($_FILES["photo"]["tmp_name"]
                                        , "../logos/"
                                        . supExt($filename)
                                        . strtotime('now')
                                        . str_replace('image/', '.', $filetype));
                                    header('Location: ../index.php');
                                    exit();
                                } elseif (substr($type, 0, 4) == "bann" || substr($type, 0, 4) == "Bann") {
                                    move_uploaded_file($_FILES["photo"]["tmp_name"]
                                        , "../banners/"
                                        . supExt($filename)
                                        . strtotime('now')
                                        . str_replace('image/', '.', $filetype));
                                    header('Location: ../index.php');
                                    exit();
                                } else {
                                    move_uploaded_file($_FILES["photo"]["tmp_name"]
                                        , "../upload/"
                                        . supExt($filename)
                                        . strtotime('now')
                                        . str_replace('image/', '.', $filetype));
                                    header('Location: ../index.php');
                                    exit();
                                }

                            } else {
                                $errors["type"] = "Erreur : Veuillez sélectionner un format de fichier valide.";
                            }

                        } else {
                            $errors["size"] = "Error: La taille du fichier est supérieure à la limite autorisée.";

                        }
                    } else {
                        $errors["format"] = "Erreur : Veuillez sélectionner un format de fichier valide.";
                    }
                } else {
                    $errors['nameFile'] = "Error: Lettres minuscules, majuscules, chiffres et le point sont autorisés, rien de plus.";

                }
            } else {
                $errors['errors'] = "Error: Vous devez télécharger une photo";
            }
        } else {
            $errors['type'] = "Error: Il faut un type d'image";
        }
    } else {
        $errors['title'] = "Error: il faut rajouter un titre, ou symbole n'est pas autorisé";
    }
}

if (!empty($errors)) {
    $_SESSION['errors'] = $errors;
    header('Location: ../index.php');
    exit();
}