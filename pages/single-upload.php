<?php
session_start();
require '../database/connexion.php';
require '../functions/functions.php';

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!--    <link rel="stylesheet" href="css/style.css">-->
    <link rel="stylesheet" href="../css/style.css">
    <link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet' type='text/css'>
    <script src="../js/jquery-3.6.0.js"></script>
    <script src="../js/slide-left.js"></script>
    <script src="../js/slide-right.js"></script>
    <title>Single miscellaneous</title>
</head>
<body>
<?php include '../navigation/header.php'; ?>
<!----- CENTER ----->
<div class="single-mid">
    <?php

    if ($_GET['name']) {
        $nameUpload = $_GET['name'];
    } else {
        header('Location: ../index.php');
        exit();
    }

    //pour récupérer l'id de là photo visé
    $uploads = $bdd->prepare("SELECT * FROM `photos` WHERE `name` = ?");
    $uploads->execute(array(
        $nameUpload,
    ));
    $upload = $uploads->fetch();

    $idUpload= $upload['id'];
    //récupération du commentaire
    /* a créer */

    ?>
    <h2><?= $upload['title'] ?></h2>
    <div class="single-align">
        <img src="../upload/<?php echo $nameUpload ?>" class="single-upload" alt="relou">
    </div>


    <div>
        <?php
        $reqComments = $bdd->prepare("SELECT * FROM comments WHERE id_photo =?");
        $reqComments->execute(array(
            $idUpload,
        ));

        while ($reqComment = $reqComments->fetch()):?>
            <div>
                <h3><?= $reqComment['name'] ?></h3>
                <p><?= $reqComment['comment'] ?></p>
            </div>

        <?php endwhile; ?>
    </div>

    <?php
    $errorCom = [];

    if (isset($_POST['envoyer'])) {

        if (is_numeric($idUpload)) {
            if (!empty($_POST['name']) and preg_match("/^[a-zA-Z0-9._ -]+$/", $_POST['name'])) {
                $name = $_POST['name'];
                if (!empty($_POST['comment'])) {
                    $comment = check_input($_POST['comment']);

                    $reqPostComment = $bdd->prepare("INSERT INTO comments (id_photo, name , comment) VALUE (?,?,?)");
                    $reqPostComment->execute(array($idUpload, $name, $comment));
                    header("Location: portfolio.php");
                    exit();
                } else {
                    echo "Il y a problème avec votre commentaire";
                }
            } else {
                echo "Il y a problème avec votre nom";
            }
        } else {
            echo 'Erreur avec la selection de l\'image';
        }

    } ?>
    <div class="single-comments">
        <form action="" method="post">
            <table>
                <caption>Laisser un commentaire</caption>
                <tr>
                    <td><label for="name">Votre nom:</label></td>
                    <td><input type="text" id="name" name="name"></td>
                </tr>
                <tr>
                    <td><label for="comment">Laisser un commentaire:</label></td>
                    <td><textarea name="comment" id="comment" cols="30" rows="10"></textarea></td>
                </tr>
                <tr>
                    <td></td>
                    <td><input type="submit" class="envoyer" value="Poster votre commentaire" name="envoyer"></td>
                </tr>
            </table>
        </form>
    </div>

</div>
<!----- RIGHT ----->
<?php require 'contact.php'; ?>

<!----- LEFT ----->
<div class="menu-wrap">
    <div class="menu-sidebar">
        <?php include '../pages/upload.php'; ?>
    </div>
</div>
<?php include "../navigation/footer.php" ?>
</body>
</html>
