<?php
session_start();
require '../database/connexion.php';
require '../functions/functions.php';
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!--    <link rel="stylesheet" href="css/style.css">-->
    <link rel="stylesheet" href="../css/style.css">
    <link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet' type='text/css'>
    <script src="../js/jquery-3.6.0.js"></script>
    <script src="../js/slide-left.js"></script>
    <script src="../js/slide-right.js"></script>
    <title>Portfolio</title>

</head>
<body>
<?php include '../navigation/header.php'; ?>
<!----- CENTER ----->
<div class="mid-upload">
    <ul class='onglet_buttons'>
        <li><a class="onglet_buttons_li" id="ongl1" href='#onglet_1'>Logos</a></li>
        <li><a class="onglet_buttons_li" id="ongl2" href='#onglet_2'>Banners</a></li>
        <li><a class="onglet_buttons_li" id="ongl3" href='#onglet_2'>Miscellaneous</a></li>
    </ul>
    <div id='onglet_1' class='onglet'>
        <?php

        $dir_nom = '../logos'; // dossier listé (pour lister le répertoir courant : $dir_nom = '.'  --> ('point')
        $dir = opendir($dir_nom) or die('Erreur de listage : le répertoire n\'existe pas'); // on ouvre le contenu du dossier courant
        $fichier = array(); // on déclare le tableau contenant le nom des fichiers
        $dossier = array(); // on déclare le tableau contenant le nom des dossiers

        while ($element = readdir($dir)) {
            if ($element != '.' && $element != '..') {
                if (!is_dir($dir_nom . '/' . $element)) {
                    $fichier[] = $element;
                } else {
                    $dossier[] = $element;
                }
            }
        }

        closedir($dir);

        if (!empty($fichier)) {
            sort($fichier);// pour le tri croissant, rsort() pour le tri décroissant
            ?>
            <h2>My creations</h2>
            <div class="ul-upload">
            <?php
            $id = 1;
            foreach ($fichier as $lien) {
                echo "<a href='../pages/single-logo.php?name=" . $lien . "' id='.$id.' class='logos_img'><img src=\"$dir_nom/$lien \" alt='creation by Nasty'></a>";
                $id++;
            }
            ?></div><?php
        }
        ?>
    </div>
    <div id='onglet_2' class='onglet'>
        <?php

        $dir_nom = '../banners'; // dossier listé (pour lister le répertoir courant : $dir_nom = '.'  --> ('point')
        $dir = opendir($dir_nom) or die('Erreur de listage : le répertoire n\'existe pas'); // on ouvre le contenu du dossier courant
        $fichier = array(); // on déclare le tableau contenant le nom des fichiers
        $dossier = array(); // on déclare le tableau contenant le nom des dossiers

        while ($element = readdir($dir)) {
            if ($element != '.' && $element != '..') {
                if (!is_dir($dir_nom . '/' . $element)) {
                    $fichier[] = $element;
                } else {
                    $dossier[] = $element;
                }
            }
        }

        closedir($dir);

        if (!empty($fichier)) {
            sort($fichier);// pour le tri croissant, rsort() pour le tri décroissant
            ?>
            <h2>My creations</h2>

            <div class="ul-upload">
            <?php
            $id = 1;
            foreach ($fichier as $lien) {
                echo "<a href='../pages/single-banner.php?name=" . $lien . "' id='.$id.' class='banners_img'><img src=\"$dir_nom/$lien \" alt='creation by Nasty'></a>";
                $id++;
            }
            ?></div><?php
        }
        ?>
    </div>
    <div id='onglet_3' class='onglet'>
        <?php

        $dir_nom = '../upload'; // dossier listé (pour lister le répertoir courant : $dir_nom = '.'  --> ('point')
        $dir = opendir($dir_nom) or die('Erreur de listage : le répertoire n\'existe pas'); // on ouvre le contenu du dossier courant
        $fichier = array(); // on déclare le tableau contenant le nom des fichiers
        $dossier = array(); // on déclare le tableau contenant le nom des dossiers

        while ($element = readdir($dir)) {
            if ($element != '.' && $element != '..') {
                if (!is_dir($dir_nom . '/' . $element)) {
                    $fichier[] = $element;
                } else {
                    $dossier[] = $element;
                }
            }
        }

        closedir($dir);

        if (!empty($fichier)) {
            sort($fichier);// pour le tri croissant, rsort() pour le tri décroissant
            ?>
            <h2>My creations</h2>

            <div class="ul-upload">
            <?php
            $id = 1;
            foreach ($fichier as $lien) {
                echo "<a href='../pages/single-upload.php?name=" . $lien . "' id='.$id.' class='upload_img'><img src=\"$dir_nom/$lien \" alt='creation by Nasty'></a>";
                $id++;
            }
            ?></div><?php
        }
        ?>
    </div>

</div>
<!----- RIGHT ----->

<?php require 'contact.php'; ?>

<!----- LEFT ----->
<div class="menu-wrap">
    <div class="menu-sidebar">
        <?php include '../pages/upload.php'; ?>
    </div>
</div>

<script>
    document.getElementById('ongl1').addEventListener('click', function () {
        document.getElementById('onglet_1').style.display = "block"
        document.getElementById('onglet_2').style.display = "none"
        document.getElementById('onglet_3').style.display = "none"
    })

    document.getElementById('ongl2').addEventListener('click', function () {
        document.getElementById('onglet_2').style.display = "block"
        document.getElementById('onglet_1').style.display = "none"
        document.getElementById('onglet_3').style.display = "none"
    })
    document.getElementById('ongl3').addEventListener('click', function () {
        document.getElementById('onglet_3').style.display = "block"
        document.getElementById('onglet_1').style.display = "none"
        document.getElementById('onglet_2').style.display = "none"
    })

</script>

<?php include "../navigation/footer.php" ?>

</body>
</html>
