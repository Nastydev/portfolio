<div class="menu-wrap-right">
    <div class="contact-top">
        <h2 id="closeContact">Contact</h2>
    </div>
    <div class="menu-sidebar-right">
        <form action="../check-forms/check-contact.php" method="post">

            <table class="form-contact">
                <tr>
                    <td><label for="name">Name  </label></td>
                    <td><input type="text" id="name" name="name" value="<?php if (isset($_SESSION['infos'])) {
                            echo htmlspecialchars($_SESSION['infos']['name']);
                        } ?>"></td>
                </tr>
                <tr>
                    <td><label for="email">Email  </label></td>
                    <td><input type="email" id="email" name="email" value="<?php if (isset($_SESSION['infos'])) {
                            echo htmlspecialchars($_SESSION['infos']['email']);
                        } ?>"></td>
                </tr>

                <tr>
                    <td><label for="subject">Subject  </label></td>
                    <td><input type="text" id="subject" name="subject" value="<?php if (isset($_SESSION['infos'])) {
                            echo htmlspecialchars($_SESSION['infos']['subject']);
                        } ?>"></td>
                </tr>

                <tr>
                    <td><label for="message">Message  </label></td>
                    <td><textarea id="message" name="message" rows="5"
                                  cols="36"><?php if (isset($_SESSION['infos'])) {
                                echo check_input($_SESSION['infos']['message']);
                            } ?></textarea></td>
                </tr>

                <tr>
                    <td></td>
                    <td><input type="submit" value="Envoyer le message" name="envoyer" class="envoyer"></td>
                </tr>
            </table>
        </form>
    </div>
</div>