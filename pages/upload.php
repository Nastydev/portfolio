    <h2>Upload Fichier</h2>
    <form action="../check-forms/check-upload-images.php" method="post" enctype="multipart/form-data">

        <table class="upload">
            <tr>
                <td><label for="title">Titre:</label></td>
                <td><input type="text" name="title" id="title"></td>
            </tr>

            <tr>
                <td><label for="type">Type:<br>(Logo; Bannière ...)</label></td>
                <td><input type="text" name="type" id="type"></td>

            </tr>

            <tr>
                <td><label for="fileUpload">Fichier:</label></td>
                <td><input type="file" name="photo" id="fileUpload"></td>
            </tr>

            <tr>
                <td></td>
                <td><input type="submit" name="submit" class="submit" value="Télécharger"></td>
            </tr>
        </table>
    </form>
