<?php
session_start();
if (isset($_SESSION['id'])) {
    $id = $_SESSION['id'];
}
if (isset($_SESSION['error'])) {
    $error = $_SESSION['error'];
} else {
    $error = NULL;
}
if (isset($_SESSION['errors'])) {
    $errors = $_SESSION['errors'];
} else {
    $errors = NULL;
}
?><!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!--    <link rel="stylesheet" href="css/style.css">-->
    <link rel="stylesheet" href="css/style.css">
    <!--        <link rel="stylesheet" href="css/background.css">-->
    <link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet' type='text/css'>
    <script src="js/jquery-3.6.0.js"></script>
    <script src="js/slide-left.js"></script>
    <script src="js/slide-right.js"></script>
    <script type="text/javascript">
        // $('html').css("position", "fixed")
    </script>
    <title>Nastywars</title>
</head>
<body>

<?php include 'navigation/header.php'; ?>
<?php include 'vote_map/vote-map.php'; ?>
<!----- CENTER ----->
<div class="mid-index" id="mid-index">
    <div id="h1"><h1>Mon titre</h1></div>
    <div id="presentation"><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto inventore ipsum
            nemo nesciunt voluptatum?
            Aliquam asperiores at cumque ducimus harum laborum minus perspiciatis repellendus sint voluptatum? At
            eaque possimus quis.</p>
    </div>
    <div>
        <!--        ERROR CONTACT-->
        <?php
        if (!empty($_SESSION['error'])): ?>
            <div class="block-center">
                <div class="block-message error"><?php echo implode('<br>', $error); ?></div>
            </div>
        <?php endif; ?>

        <!--        ERROR UPLOAD-->
        <?php
        if (!empty($_SESSION['errors'])): ?>
            <div class="block-center">
                <div class="block-message error"><?php echo implode('<br>', $errors); ?></div>
            </div>
        <?php endif; ?>

        <!--        SUCESS CONTACT-->
        <?php
        if (isset($_SESSION['sucess']) == 1): ?>
            <div class="block-center">
                <div class="block-message sucess">
                    <p>Votre message à bien été envoyé!</p>
                </div>
            </div>
        <?php endif; ?>
    </div>
</div>
<!----- RIGHT ----->
<div class="menu-wrap-right">
    <div>
        <h2 id="closeContact">Contact</h2>
    </div>
    <div class="right">
        <form action="check-forms/check-contact.php" method="post">
            <?php require 'functions/functions.php' ?>

            <table class="form-contact">
                <tr>
                    <td><label for="name">Name  </label></td>
                    <td><input type="text" id="name" name="name" value="<?php if (isset($_SESSION['infos'])) {
                            echo htmlspecialchars($_SESSION['infos']['name']);
                        } ?>"></td>
                </tr>
                <tr>
                    <td><label for="email">Email  </label></td>
                    <td><input type="email" id="email" name="email" value="<?php if (isset($_SESSION['infos'])) {
                            echo htmlspecialchars($_SESSION['infos']['email']);
                        } ?>"></td>
                </tr>

                <tr>
                    <td><label for="subject">Subject  </label></td>
                    <td><input type="text" id="subject" name="subject" value="<?php if (isset($_SESSION['infos'])) {
                            echo htmlspecialchars($_SESSION['infos']['subject']);
                        } ?>"></td>
                </tr>

                <tr>
                    <td><label for="message">Message  </label></td>
                    <td><textarea id="message" name="message" rows="5"
                                  cols="36"><?php if (isset($_SESSION['infos'])) {
                                echo check_input($_SESSION['infos']['message']);
                            } ?></textarea></td>
                </tr>

                <tr>
                    <td></td>
                    <td><input type="submit" value="Envoyer le message" name="envoyer" class="envoyer"></td>
                </tr>
            </table>
        </form>

    </div>

</div>

<!----- LEFT ----->
<div class="menu-wrap">
    <div class="menu-sidebar">
        <?php if (isset($_SESSION['id'])) {
            include 'pages/upload.php';
        } ?>

    </div>
</div>
<footer>
    <div>
        <p >Nastywars &copy; 2021</p>
    </div>
    <footer>
        <div id="logo" class="footer_img">
            <a  href="https://www.linkedin.com/in/c%C3%A9dric-tourneux/"><img src="images/linkedin.svg" alt="linkedin"></a>
            <p >Nastywars &copy; 2021</p>
            <a  href="https://twitter.com/Nastywars_"><img src="images/twitter.svg" alt="twitter"></a>
        </div>
    </footer>
</footer>
</body>
</html>
<?php
unset($_SESSION['infos']);
unset($_SESSION['error']); //unset error de contact
unset($_SESSION['errors']); //unset error de upload
unset($_SESSION['message']);
unset($_SESSION['sucess']);
?>