<?php
function check_input($check)
{
    $check = trim($check);
    $check = stripslashes($check);
    $check = htmlspecialchars($check);
    $check = preg_replace("/\s+/", " ", $check);
    return $check;
}

function SupExt ($file_name) {
    $supPoint = ".";
    $position = strpos($file_name, $supPoint);
    return substr($file_name, 0, $position);
}