<?php
session_start();
if (isset($_SESSION['error'])){
    $test = $_SESSION['error'];
}

require('../database/connexion.php');

$error = [];

if (isset($_POST['submit'])) {
    if (!empty($_POST['pseudo'])) {
        if (preg_match("/^[a-zA-Z ]+$/", $_POST['pseudo'])) {
            if (!empty($_POST['password'])) {
                if (preg_match("/^[0-9a-zA-Z\- \/_?:.,\s]+$/", $_POST['password'])) {
                    $nastydev = $_POST['pseudo'];
                    $reqPseudos = $bdd->prepare("SELECT * FROM nastydev_admin WHERE user = ? ");
                    $reqPseudos->execute(array($nastydev));
                    $reqPseudo = $reqPseudos->rowCount();

                    $reqPassword = $bdd->prepare("SELECT * FROM nastydev_admin WHERE user = ?");
                    $reqPassword->execute(array($nastydev));
                    $userPassword = $reqPassword->fetch();
                    $az = password_verify($_POST['password'], $userPassword['password']);
                    if ($az === true) {
                        $_SESSION['id'] = md5(time() . mt_rand(1,10000000));
                        var_dump( $_SESSION['id']);
                        header("Location: ../index.php");
                        exit();
                    } else {
                        $error = ["Le mot de passe ne correspond pas."];
                    }
                } else {
                    $error = ["Mauvais caractère dans le mot de passe."];
                }
            } else {
                $error = ["Il faut un mot de passe."];
            }
        } else {
            $error = ["Contenu indésirable dans le pseudo."];
        }
    } else {
        $error = ["Il faut un nom d'utilisateur."];
    }

}
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>login</title>
</head>
<body>

<?php if (!empty($_SESSION['error'])): implode('<br>', $test); endif; ?>

<form method="post">

    <label for="pseudo">Pseudo</label>
    <input type="text" name="pseudo" id="pseudo"><br>

    <label for="password">Password</label>
    <input type="password" id="password" name="password"><br>

    <input type="submit" name="submit">
</form>

</body>
</html>
