
<script>
    jQuery(function () {
        $(function () {
            $(window).scroll(function () {
                $(this).scrollTop()
            });
        });
    });
</script>
<footer>
    <div id="scrollUp">
        <a href="#"><img src="../to_top.png"/></a>
    </div>
    <div id="logo" class="footer_img">
        <a  href="https://www.linkedin.com/in/c%C3%A9dric-tourneux/"><img src="../images/linkedin.svg" alt="linkedin"></a>
        <p >Nastywars &copy; 2021</p>
        <a  href="https://twitter.com/Nastywars_"><img src="../images/twitter.svg" alt="twitter"></a>
    </div>
</footer>
